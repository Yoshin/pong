import javax.swing.JFrame;


public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) { //Entry point du programme (Le main() quoi ^^)
		JFrame frm = new JFrame(); //On cr�e la fenetre
		frm.setTitle("Pong"); //On nome la fenetre
		Game g = new Game(); //On cr�e une instance de jeu
		frm.setContentPane(g); //On applique le jeu sur la fenetre
		frm.setSize(500, 700); //On donne la taille a la fenetre
		frm.setResizable(false); //On fais en sorte que on ne puisse pas la redimenssioner
		frm.setVisible(true); //On la rend visible
		frm.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); //Choix de l'acrion qui sui le clic sur la crois (dans notre cas, on ferme le programme)
	}

}
