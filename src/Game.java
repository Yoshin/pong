import java.awt.*;
import java.awt.event.*;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;
import java.util.HashSet;

import javax.swing.*;


public class Game extends JPanel implements KeyListener, ActionListener {
	
	// D�claration des variables de la classe
	private int height, width; //Taille de la fenetre
	private Timer t = new Timer(5, this); //Timer pour la vitesse du jeu
	private boolean first; //Bol�en de d�claration (Voir plus bas)
	
	private HashSet<String> keys = new HashSet<String>(); //Tableu de "key" (Voir plus bas)
	
	// Variables des Barres
	private int speed = 5; //Vitesse du/des joueurs
	private int iaspeed = 2; //Vitesse de l'IA
	private int barH = 10, barW = 70; //Tailles des barres
	private int bottomBarX, topBarX; //Position des barres
	private int inset = 40; //Marge du terain heut et bas
	private int gameType = 0; //Type de game (PvP PvIA)
	private String diff = "Normal"; //Nom de la dificult�e
	
	// Variables de la Balle
	private double ballX, ballY, velX = 2, velY = 2, ballSize = 20;
	
	// Variables des Scores
	private int scoreIA, scorePlayer;
	
	//Fonction de lancement du jeu
	public Game() {
		addKeyListener(this); //le KeyListener permet de d�tecter l'appuie des touches
		setFocusable(true); //La fenetre devient focusable (elle d�tecte quand la souris est dessus ou sur un autre programme)
		setFocusTraversalKeysEnabled(false); //On d�sactive la d�tection des touches meme quand la fenetre n'est pas au premier plan
		first = true; //Bol�en qui va servir a initialiser le jeu
		t.setInitialDelay(100); //On cr�e un timer pour la vitesse du jeu
		t.start(); //On lance le timer
	}
	
	//Fonction d'�criture graphique
	@Override
	protected void paintComponent(Graphics g) { //"Graphics g" repr�sente un fenetre java classique 
		super.paintComponent(g); //On autorise l'�criture sur cet �l�ment
		Graphics2D g2d = (Graphics2D) g; //On cr�e une interface graphique sur la fenetre
		height = getHeight(); //On d�finis la taille de l'image en fonction de la taille de la fenetre
		width = getWidth(); //

		//Premiere boucle d'initialisation
		//Si "first = true" on renre dans la fonction ce qui permet d'initialiser la table de jeu
		if (first) {
			bottomBarX = width / 2 - barW / 2;
			topBarX = bottomBarX;
			ballX = width / 2 - ballSize / 2;
			ballY = height / 2 - ballSize / 2;
			first = false; //On passe first a false pour ne pas repasser dans la fonction a l'avenir
		}
		
		//On dessine la barre d'en bas
		Rectangle2D bottomBar = new Rectangle(bottomBarX, height - barH - inset, barW, barH); //On cr�e l'objet
		g2d.fill(bottomBar); //On l'ajoute l'�llement a l'image
		
		//Celle d'en haut
		Rectangle2D topBar = new Rectangle(topBarX, inset, barW, barH);
		g2d.fill(topBar);
		
		//La balle
		Ellipse2D ball = new Ellipse2D.Double(ballX, ballY, ballSize, ballSize);
		g2d.fill(ball);
		
		//Les trois barres horizontales
		Rectangle2D barMid = new Rectangle(0, height / 2, width, 2);
		g2d.fill(barMid);
		Rectangle2D barTop = new Rectangle(0, 30, width, 5);
		g2d.fill(barTop);
		Rectangle2D barBot = new Rectangle(0, height - inset + 5, width, 5);
		g2d.fill(barBot);
		
		
		//Les textes (scores, difficult� et aide)
		String scoreB = "Player 1 : " + new Integer(scorePlayer).toString();
		String scoreT = (gameType == 0) ? ("IA : " + new Integer(scoreIA).toString()) : ("Player 2 : " + new Integer(scoreIA).toString());
		String dif = "Difficulty : " + diff;
		String howTo = "N : New Game   1,2,3 : Select dificulty   I/P : Play vs (I)A or (P)layer (P2 controls: W and X)";
		g2d.drawString(scoreB, 10, 20);
		g2d.drawString(scoreT, width - 70, 20);
		g2d.drawString(dif, (width / 2) - 50, 20);
		g2d.drawString(howTo, 7, height - 10);
		
		//Tout ca aplique le graphisme sur l'image. On aposera l'image apres les calculs
		//de trajectoires et de mouvement. Ca sera la fonction repaint() plus bas
	}
	
	//Fonction de d�tection des actions et des mouvement
	@Override
	public void actionPerformed(ActionEvent e) {
		//Calcul des rebons sur les murs droites et gauche
		if (ballX < 0 || ballX > width - ballSize) {
			velX = -velX;
		}
		//Calcul des rebons sur le mur haut
		if (ballY < inset) {
			velY = -velY;
			++ scorePlayer; //Si il ya colision, on ajoute le score
		}
		//Calcul des rebons sur le mur bas
		if (ballY + ballSize > height - inset) {
			velY = -velY;
			++ scoreIA;
		}
		//Calcul des colisions sur la barre basse
		if (ballY + ballSize >= height - barH - inset && velY > 0)
			if (ballX + ballSize >= bottomBarX && ballX <= bottomBarX + barW)
				velY = -velY;

		//Calcul des colisions sur la barre hautte
		if (ballY <= barH + inset && velY < 0)
			if (ballX + ballSize >= topBarX && ballX <= topBarX + barW)
				velY = -velY;
		
		//Nouvelle position de la balle en fonction du d�placement
		ballX += velX;
		ballY += velY;
		
		//Gestion des touches de mouvement
		if (keys.size() == 1) {
			//"keys.size() == 1" On regarde si il y a des touches dans le tableau **Voir gestion des touches plus bas**
			if (keys.contains("LEFT")) { //"keys.size() == 1" On regarde si "LEFT" est dans le tableau **Voir gestion des touches plus bas**
				bottomBarX -= (bottomBarX > 0) ? speed : 0;
			}
			else if (keys.contains("RIGHT")) {
				bottomBarX += (bottomBarX < width - barW) ? speed : 0;
			}
		}
		if (gameType == 1) { //Si le type de game est PvP on d�tecte aussi les touches du joueur 2
			if (keys.size() == 1) {
				if (keys.contains("W")) {
					topBarX -= (topBarX > 0) ? speed : 0;
				}
				else if (keys.contains("X")) {
					topBarX += (topBarX < width - barW) ? speed : 0;
				}
			}
		} else {
		//Sinon, on fait bouger l'IA
			double moove = ballX - topBarX;
			if (moove > 0) {
				topBarX += (topBarX < width - barW) ? iaspeed : 0;
			}
			else if (moove < 0) {
				topBarX -= (topBarX > 0) ? iaspeed : 0;
			}
		}
		
		repaint(); //Apres calcul, et d�placement, on redessine l'image sur la fenetre pour visualiser les changements
	}

	//Fonction de gestion des autres touches (Voir plus bas pour comment on en arrive a cette fonction)
	public void chgOpts(int opts) {
		if (opts == 0) {		//(opts == 0) => Changement du mode en mode PvP
			gameType = 1;		//Changement de la variable du mode pour les calculs
			scoreIA = 0;		//Remise a zero des scores car changement du mode
			scorePlayer = 0;	//Remise a zero des scores car changement du mode
		}
		else if (opts == 1) {	//(opts == 1) => Changement du mode en mode PvIA
			gameType = 0;		//
			scoreIA = 0;		//Idem que au dessus
			scorePlayer = 0;	//
		}
		else if (opts == 2) { 	//(opts == 2) => Changement de la dificult� en "facile"
			diff = "Easy";		//Changement du nom de la dificult�e
			velX = 1;			//Changement de la rapidit� de la balle en X
			velY = 1;			//Changement de la rapidit� de la balle en Y
			speed = 2;			//Changement de la rapidit� des bares joueurs
			iaspeed = speed - 1; //Changement de la rapidit� de la bare IA
			scoreIA = 0;		//Remise a zero des scores car changement de la dificult�e
			scorePlayer = 0; 	//Remise a zero des scores car changement de la dificult�e
		}
		else if (opts == 3) { //(opts == 3) => Changement de la dificult� en "normal"
			diff = "Normal";
			velX = 2;
			velY = 2;
			speed = 5;
			iaspeed = speed - 3;
			scoreIA = 0;
			scorePlayer = 0;
		}
		else if (opts == 4) { //(opts == 4) => Changement de la dificult� en "dificile"
			diff = "Hard";
			velX = 5;
			velY = 5;
			speed = 7;
			iaspeed = speed - 3;
			scoreIA = 0;
			scorePlayer = 0;
		}
	}

	//Fonction pour l'utilisation du keyListener
	@Override
	public void keyTyped(KeyEvent e) {}
	
	//Fonction de gestion des effets des touches
	@Override
	public void keyPressed(KeyEvent e) {
		int code = e.getKeyCode(); //On r�cupere le nom de la touche qui est appuy� dans une variable
		/* Pour les 4 touches de d�placement, on dois gerer la r�p�tition.
		** Pour ca, on fait la dif�rence entre touche appuy� et touche relev�.
		** Des que une touche de d�placement est appuy�, on la stoque dans un table
		** (dans un "HashSet de strings" en r�alit�, c'est un genre de tableau dynamique) ;
		** comme le programme boucle, si le nom de la touche est pr�sente dans le tableau
		** on fais le d�placement (Voir plus haut la ou j'ai mis "Voir gestion des touches plus bas")
		** et tant que elle est pr�sente a chaque boucle on red�place
		** Plus bas, si la touche est relach�, on la suprimera du tableau, et donc
		** on ne fera plus le d�placement.
		**/
		switch (code) {
		case KeyEvent.VK_LEFT:
			keys.add("LEFT");
			break;
		case KeyEvent.VK_RIGHT:
			keys.add("RIGHT");
			break;
		case KeyEvent.VK_W:
			keys.add("W");
			break;
		case KeyEvent.VK_X:
			keys.add("X");
			break;
		case KeyEvent.VK_N: //Si "N" est pr�s�e, on remet les scores a 0 pour une nouvelle partie
			scoreIA = 0;
			scorePlayer = 0;
			break;
		case KeyEvent.VK_NUMPAD1: //1, 2 et 3 du clavier num�rique servent a changer la dificult�e
			chgOpts(2);
			break;
		case KeyEvent.VK_NUMPAD2:
			chgOpts(3);
			break;
		case KeyEvent.VK_NUMPAD3:
			chgOpts(4);
			break;
		case KeyEvent.VK_P: //P et I servent a chganger le mode de jeu 
			chgOpts(0);
			break;
		case KeyEvent.VK_I:
			chgOpts(1);
			break;
		}
	}

	@Override
	public void keyReleased(KeyEvent e) {
		int code = e.getKeyCode();
		/* Seconde partie de la gestion des touches. Comme j'ai dit plus haut,
		** si la touche est relev�e, on la retire du tableau.
		**/
		switch (code) {
		case KeyEvent.VK_LEFT:
			keys.remove("LEFT");
			break;
		case KeyEvent.VK_RIGHT:
			keys.remove("RIGHT");
			break;
		case KeyEvent.VK_W:
			keys.remove("W");
			break;
		case KeyEvent.VK_X:
			keys.remove("X");
			break;
		}
	}
}
